import http from 'http';
import { EventEmitter } from 'events';

import app from '../src/app';
import SocketIo from '../src/config/socketio';

const port = normalizePort(process.env.PORT || '9000');
const server = http.createServer(app);
const io = new SocketIo(server);
const eventEmitter = new EventEmitter();

eventEmitter.on('upload', () => {
  io.upload();
});

app.set('port', port);
server.listen(port, () => {
  console.log(`Running in port: ${port}`);
});

/**
 * Normalize a port into a number, string, or false.
 */
function normalizePort(val) {
  const port = parseInt(val, 10);

  if (isNaN(port)) {
    return val;
  }

  if (port >= 0) {
    return port;
  }

  return false;
}

export { eventEmitter };
