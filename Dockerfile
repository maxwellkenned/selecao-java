FROM node:alpine

WORKDIR /usr/app/api

COPY package*.json ./

RUN set NODE_OPTIONS=--max-old-space-size=4096
RUN npm install

COPY . .


EXPOSE 9000

CMD ["npm", "start", "--node-flags", "--max-old-space-size=4096"]