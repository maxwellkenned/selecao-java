import { readdirSync } from 'fs';
import { resolve } from 'path';

import connection from '../config/connection';

const pathModels = resolve(__dirname, '..', 'app', 'models');
const Models = readdirSync(pathModels);

const models = [];

Models.map(model => {
  const [modelName] = model.split('.');
  const Model = require(resolve(pathModels, modelName));

  models.push(Model.default);
});

class Database {
  constructor() {
    this.init();
  }

  init() {
    this.connection = connection;

    models
      .map(model => model.init(this.connection))
      .map(model => model.associate && model.associate(this.connection.models));
  }
}

export default new Database();
