'use strict';

module.exports = {
  up: queryInterface => {
    return queryInterface.addConstraint('cities', ['name', 'state_id'], {
      type: 'unique',
      name: 'cities_unique_constraint_name_state',
    });
  },

  down: queryInterface => {
    return queryInterface.removeConstraint(
      'cities',
      'cities_unique_constraint_name_state',
    );
  },
};
