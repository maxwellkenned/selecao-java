'use strict';

module.exports = {
  up: queryInterface => {
    return queryInterface.addConstraint('distributors', ['name', 'city_id'], {
      type: 'unique',
      name: 'distributors_unique_constraint_name_city',
    });
  },

  down: queryInterface => {
    return queryInterface.removeConstraint(
      'distributors',
      'distributors_unique_constraint_name_city',
    );
  },
};
