'use strict';

module.exports = {
  up: queryInterface => {
    return queryInterface.addConstraint(
      'price_history',
      [
        'collection_date',
        'product_id',
        'distributor_id',
        'city_id',
        'provider_id',
      ],
      {
        type: 'unique',
        name: 'price_history_unique_constraint_product__distributor_city_provider',
      },
    );
  },

  down: queryInterface => {
    return queryInterface.removeConstraint(
      'price_history',
      'price_history_unique_constraint_product__distributor_city_provider',
    );
  },
};
