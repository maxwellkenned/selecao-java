'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('price_history', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      dist_product_id: Sequelize.INTEGER,
      collection_date: Sequelize.DATE,
      purchase_price: Sequelize.DOUBLE,
      sale_price: Sequelize.DOUBLE,
      unit: Sequelize.STRING,
      product_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'products',
          key: 'id',
        },
      },
      distributor_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'distributors',
          key: 'id',
        },
      },
      provider_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'providers',
          key: 'id',
        },
      },
      city_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'cities',
          key: 'id',
        },
      },
      created_at: Sequelize.DATE,
      updated_at: Sequelize.DATE,
    });
  },

  down: queryInterface => {
    return queryInterface.dropTable('price_history');
  },
};
