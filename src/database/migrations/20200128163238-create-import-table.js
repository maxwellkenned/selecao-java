'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('imports', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      region: Sequelize.STRING,
      state: Sequelize.STRING,
      city: Sequelize.STRING,
      distributor: Sequelize.STRING,
      dist_product_id: Sequelize.STRING,
      product: Sequelize.STRING,
      collection_date: Sequelize.STRING,
      purchase_price: Sequelize.STRING,
      sale_price: Sequelize.STRING,
      unit: Sequelize.STRING,
      provider: Sequelize.STRING,
      created_at: Sequelize.DATE,
      updated_at: Sequelize.DATE,
    });
  },

  down: queryInterface => {
    return queryInterface.dropTable('imports');
  },
};
