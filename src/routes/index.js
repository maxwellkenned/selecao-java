import { Router } from 'express';
import multer from '../app/utils/multerStorageCsv';

import swaggerUi from 'swagger-ui-express';
import swaggerConfig from '../config/swaggerConfig';

import authMiddleware from '../app/middlewares/auth';

import ImportController from '../app/controllers/ImportController';
import UserController from '../app/controllers/UserController';
import ProductController from '../app/controllers/ProductController';
import RegionController from '../app/controllers/RegionController';
import ProviderController from '../app/controllers/ProviderController';
import StateController from '../app/controllers/StateController';
import CityController from '../app/controllers/CityController';
import DistributorController from '../app/controllers/DistributorController';
import SessionController from '../app/controllers/SessionController';
import PriceHistoryController from '../app/controllers/PriceHistoryController';

const routes = Router();

/** API DOC */
routes.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerConfig));

/** Create user */
routes.post('/users', UserController.store);

/** Create */
routes.post('/sessions', SessionController.store);

/** setting authentication on the routes below */
routes.use(authMiddleware);

/** import csv */
routes.post('/import', multer.single('file'), ImportController.store);

/** User routes */
routes.get('/users', UserController.index);
routes.put('/users', UserController.update);
routes.delete('/users', UserController.delete);

/** Product routes */
routes.get('/products', ProductController.index);
routes.post('/products', ProductController.store);
routes.put('/products', ProductController.update);
routes.delete('/products', ProductController.delete);

/** Region routes */
routes.get('/regions', RegionController.index);
routes.post('/regions', RegionController.store);
routes.put('/regions', RegionController.update);
routes.delete('/regions', RegionController.delete);

/** Provider routes */
routes.get('/providers', ProviderController.index);
routes.post('/providers', ProviderController.store);
routes.put('/providers', ProviderController.update);
routes.delete('/providers', ProviderController.delete);

/** State routes */
routes.get('/states', StateController.index);
routes.post('/states', StateController.store);
routes.put('/states', StateController.update);
routes.delete('/states', StateController.delete);

/** City routes */
routes.get('/cities', CityController.index);
routes.post('/cities', CityController.store);
routes.put('/cities', CityController.update);
routes.delete('/cities', CityController.delete);

/** Distributor routes */
routes.get('/distributors', DistributorController.index);
routes.post('/distributors', DistributorController.store);
routes.put('/distributors', DistributorController.update);
routes.delete('/distributors', DistributorController.delete);

/** Price History routes */
routes.get('/pricehistories', PriceHistoryController.index);
routes.post('/pricehistories', PriceHistoryController.store);
routes.put('/pricehistories', PriceHistoryController.update);
routes.delete('/pricehistories', PriceHistoryController.delete);
routes.get(
  '/pricehistories/avg/city',
  PriceHistoryController.getAvgPerCityName,
);
routes.get('/pricehistories/region', PriceHistoryController.getAllPerRegion);
routes.get(
  '/pricehistories/distributor',
  PriceHistoryController.getAllPerDistributor,
);
routes.get(
  '/pricehistories/collectiondate',
  PriceHistoryController.getAllPerCollectionDate,
);
routes.get('/pricehistories/cities/avg', PriceHistoryController.getAvgPerCity);
routes.get(
  '/pricehistories/providers/avg',
  PriceHistoryController.getAvgPerProvider,
);

export default routes;
