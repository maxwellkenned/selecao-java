import AbstractError from './AbstractError';

export default class RegionError extends AbstractError {
  constructor(message = 'Região não encontrada', status = 404) {
    super(message, status);
  }
}
