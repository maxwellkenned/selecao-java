import AbstractError from './AbstractError';

export default class CityError extends AbstractError {
  constructor(message = 'Cidade não encontrada', status = 404) {
    super(message, status);
  }
}
