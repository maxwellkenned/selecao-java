import AbstractError from './AbstractError';

export default class StateError extends AbstractError {
  constructor(message = 'Estado não encontrado', status = 404) {
    super(message, status);
  }
}
