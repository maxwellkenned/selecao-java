import AbstractError from './AbstractError';

export default class DistributorError extends AbstractError {
  constructor(message = 'Distribuidor não encontrada', status = 404) {
    super(message, status);
  }
}
