import AbstractError from './AbstractError';

export default class ProductError extends AbstractError {
  constructor(message = 'Produto não encontrado', status = 404) {
    super(message, status);
  }
}
