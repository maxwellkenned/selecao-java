import AbstractError from './AbstractError';

export default class ImportError extends AbstractError {
  constructor(
    message = 'Ocorreu um erro ao importar o arquivo.',
    status = 500,
  ) {
    super(message, status);
  }
}
