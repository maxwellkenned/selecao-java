import AbstractError from './AbstractError';

export default class UserError extends AbstractError {
  constructor(message = 'Usuário não encontrado', status = 404) {
    super(message, status);
  }
}
