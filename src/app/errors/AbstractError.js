export default class AbstractError {
  constructor(message = 'Ocorreu um erro na aplicação', status = 500) {
    this.message = message;
    this.status = status;
  }
}
