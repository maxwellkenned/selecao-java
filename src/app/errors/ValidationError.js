import AbstractError from './AbstractError';

export default class ValidationError extends AbstractError {
  constructor(message = 'Validação falhou. Verifique as informações!', status = 400) {
    super(message, status);
  }
}
