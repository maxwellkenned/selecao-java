import AbstractError from './AbstractError';

export default class PriceHistoryError extends AbstractError {
  constructor(message = 'Histórico de preço não encontrado', status = 404) {
    super(message, status);
  }
}
