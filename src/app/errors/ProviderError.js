import AbstractError from './AbstractError';

export default class ProviderError extends AbstractError {
  constructor(message = 'Fornecedor não encontrado', status = 404) {
    super(message, status);
  }
}
