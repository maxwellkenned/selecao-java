import * as Yup from 'yup';

import ValidationError from '../errors/ValidationError';
import validMessage from '../utils/validationsMessage';

class UserValidator {
  async createValidation(user) {
    try {
      const schema = Yup.object().shape({
        name: Yup.string().required(validMessage.required),
        email: Yup.string()
          .email(validMessage.email)
          .required(validMessage.required),
        password: Yup.string()
          .required(validMessage.required)
          .min(6, validMessage.min),
      });

      await schema.validate(user);
    } catch (error) {
      console.log(error);
      throw new ValidationError(error.message);
    }
  }

  async updateValidation(user) {
    try {
      const schema = Yup.object().shape({
        name: Yup.string(),
        email: Yup.string().email(validMessage.email),
        oldPassword: Yup.string().min(6, validMessage.min),
        password: Yup.string()
          .min(6, validMessage.min)
          .when('oldPassword', (oldPassword, field) =>
            oldPassword ? field.required(validMessage.required) : field,
          ),
        confirmPassword: Yup.string().when('password', (password, field) =>
          password
            ? field.required(validMessage.required).oneOf([Yup.ref('password')])
            : field,
        ),
      });

      await schema.validate(user);
    } catch (error) {
      throw new ValidationError(error.message);
    }
  }
}

export default new UserValidator();
