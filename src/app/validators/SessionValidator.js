import * as Yup from 'yup';

import ValidationError from '../errors/ValidationError';
import validMessage from '../utils/validationsMessage';

class SessionValidator {
  async createValidation(user) {
    try {
      const schema = Yup.object().shape({
        email: Yup.string()
          .email(validMessage.email)
          .required(validMessage.required),
        password: Yup.string().required(validMessage.required),
      });

      await schema.validate(user);
    } catch (error) {
      throw new ValidationError(error.message);
    }
  }
}

export default new SessionValidator();
