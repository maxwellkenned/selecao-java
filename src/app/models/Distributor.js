import { Model, DataTypes } from 'sequelize';

class Distributor extends Model {
  static init(sequelize) {
    super.init(
      {
        name: DataTypes.STRING,
        city_id: DataTypes.INTEGER,
      },
      { sequelize, tableName: 'distributors', modelName: 'Distributor' },
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.City, { foreignKey: 'city_id' });
  }

  static async createOrFind(data, options) {
    let result = await this.findOne({
      where: data,
      ...options,
    });

    if (!result) {
      result = await this.create(data, options);
    }

    return result;
  }
}

export default Distributor;
