import { Model, DataTypes } from 'sequelize';

class City extends Model {
  static init(sequelize) {
    super.init(
      {
        name: DataTypes.STRING,
        state_id: DataTypes.INTEGER,
      },
      { sequelize, tableName: 'cities', modelName: 'City' },
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.State, { foreignKey: 'state_id' });
  }

  static async createOrFind(data, options) {
    let result = await this.findOne({
      where: data,
      ...options,
    });

    if (!result) {
      result = await this.create(data, options);
    }

    return result;
  }
}

export default City;
