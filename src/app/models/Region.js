import { Model, DataTypes } from 'sequelize';

class Region extends Model {
  static init(sequelize) {
    super.init(
      {
        name: DataTypes.STRING,
      },
      { sequelize, tableName: 'regions', modelName: 'Region' },
    );

    return this;
  }

  static async createOrFind(data, options) {
    let result = await this.findOne({
      where: data,
      ...options
    });
    
    if (!result) {
      result = await this.create(data, options);
    };

    return result;
  }
}

export default Region;
