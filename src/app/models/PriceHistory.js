import { Model, DataTypes } from 'sequelize';

class PriceHistory extends Model {
  static init(sequelize) {
    super.init(
      {
        dist_product_id: DataTypes.INTEGER,
        collection_date: DataTypes.DATE,
        purchase_price: DataTypes.DOUBLE,
        sale_price: DataTypes.DOUBLE,
        unit: DataTypes.STRING,
        product_id: DataTypes.INTEGER,
        distributor_id: DataTypes.INTEGER,
        provider_id: DataTypes.INTEGER,
        city_id: DataTypes.INTEGER,
      },
      { sequelize, tableName: 'price_history', modelName: 'PriceHistory' },
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.Product, { foreignKey: 'product_id' });
    this.belongsTo(models.Distributor, { foreignKey: 'distributor_id' });
    this.belongsTo(models.Provider, { foreignKey: 'provider_id' });
    this.belongsTo(models.City, { foreignKey: 'city_id' });
  }
}

export default PriceHistory;
