import { Model, DataTypes } from 'sequelize';

class Import extends Model {
  static init(sequelize) {
    super.init(
      {
        region: DataTypes.STRING,
        state: DataTypes.STRING,
        city: DataTypes.STRING,
        distributor: DataTypes.STRING,
        dist_product_id: DataTypes.STRING,
        product: DataTypes.STRING,
        collection_date: DataTypes.STRING,
        purchase_price: DataTypes.STRING,
        sale_price: DataTypes.STRING,
        unit: DataTypes.STRING,
        provider: DataTypes.STRING,
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE,
      },
      { sequelize, tableName: 'imports', modelName: 'Import' },
    );

    return this;
  }
}

export default Import;
