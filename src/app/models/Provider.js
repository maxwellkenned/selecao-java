import { Model, DataTypes } from 'sequelize';

class Provider extends Model {
  static init(sequelize) {
    super.init(
      {
        name: DataTypes.STRING,
      },
      { sequelize, tableName: 'providers', modelName: 'Provider' },
    );

    return this;
  }
}

export default Provider;
