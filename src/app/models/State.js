import { Model, DataTypes } from 'sequelize';

class State extends Model {
  static init(sequelize) {
    super.init(
      {
        name: DataTypes.STRING,
        region_id: DataTypes.INTEGER,
      },
      { sequelize, tableName: 'states', modelName: 'State' },
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.Region, { foreignKey: 'region_id' });
  }

  static async createOrFind(data, options) {
    let result = await this.findOne({
      where: data,
      ...options,
    });

    if (!result) {
      result = await this.create(data, options);
    }

    return result;
  }
}

export default State;
