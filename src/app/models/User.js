import { Model, DataTypes } from 'sequelize';
import { hash, compare } from 'bcryptjs';

class User extends Model {
  static init(sequelize) {
    super.init(
      {
        name: DataTypes.STRING,
        email: DataTypes.STRING,
        password: DataTypes.STRING,
      },
      { sequelize, tableName: 'users', modelName: 'User' },
    );

    this.addHook('beforeSave', async user => {
      if (user.password) {
        user.password = await hash(user.password, 8);
      }
    });

    return this;
  }

  checkPassword(password) {
    return compare(password, this.password);
  }
}

export default User;
