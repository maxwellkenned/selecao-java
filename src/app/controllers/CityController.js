import CityService from '../services/CityService';

class CityController {
  async index(req, res) {
    try {
      const { page, limit, offset } = req.query;

      const [maxPages, result] = await CityService.getAll(page, limit, offset);

      return res.json({ page: page || 1, maxPages, result });
    } catch (error) {
      console.log(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }

  async store(req, res) {
    try {
      const { name, state_id } = req.body;

      const city = await CityService.create({ name, state_id });

      return res.json(city);
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }

  async update(req, res) {
    try {
      const { city_id, name } = req.body;

      const city = await CityService.update({ city_id, name });

      return res.json(city);
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }

  async delete(req, res) {
    try {
      const { city_id } = req.body;

      const city = await CityService.delete({ city_id });

      return res.json({ message: 'Cidade excluído com sucesso!' });
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }
}

export default new CityController();
