import UserService from '../services/UserService';

class UserController {
  async index(req, res) {
    try {
      const { page, limit, offset } = req.query;

      const [maxPages, result] = await UserService.getAll(page, limit, offset);

      return res.json({ page: page || 1, maxPages, result });
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }

  async store(req, res) {
    try {
      const { name, email, password } = req.body;

      const user = await UserService.create({ name, email, password });

      return res.json(user);
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }

  async update(req, res) {
    try {
      const { name, email, password, oldPassword, confirmPassword } = req.body;
      const userId = req.userId;

      const user = await UserService.update({
        userId,
        name,
        email,
        password,
        oldPassword,
        confirmPassword,
      });

      return res.json(user);
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }

  async delete(req, res) {
    try {
      const userId = req.userId;

      const user = await UserService.delete({ userId });

      return res.json({ message: 'Usuário excluído com sucesso!' });
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }
}

export default new UserController();
