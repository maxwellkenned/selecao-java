import SessionService from '../services/SessionService';

class SessionController {
  async store(req, res) {
    try {
      const { email, password } = req.body;

      const token = await SessionService.createSession(email, password);

      if (token.error) {
        return res.status(401).json(token);
      }

      return res.json(token);
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }
}

export default new SessionController();
