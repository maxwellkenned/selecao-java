import DistributorService from '../services/DistributorService';

class DistributorController {
  async index(req, res) {
    try {
      const { page, limit, offset } = req.query;

      const [maxPages, result] = await DistributorService.getAll(
        page,
        limit,
        offset,
      );

      return res.json({ page: page || 1, maxPages, result });
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }

  async store(req, res) {
    try {
      const { name, city_id } = req.body;

      const distributor = await DistributorService.create({ name, city_id });

      return res.json(distributor);
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }

  async update(req, res) {
    try {
      const { distributor_id, name } = req.body;

      const distributor = await DistributorService.update({
        distributor_id,
        name,
      });

      return res.json(distributor);
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }

  async delete(req, res) {
    try {
      const { distributor_id } = req.body;

      const distributor = await DistributorService.delete({ distributor_id });

      return res.json({ message: 'Distribuidor excluído com sucesso!' });
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }
}

export default new DistributorController();
