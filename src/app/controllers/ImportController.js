import ImportService from '../services/ImportService';

class ImportController {
  store(req, res) {
    try {
      const file = req.file;

      if (!file) {
        return res.status(415).json({
          message: 'Formato Inválido. Por favor, envie um csv válido!',
        });
      }

      const response = ImportService.importCsv(file);

      return res.json({
        message: `Arquivo importado com sucesso.
        Será realizado a importação dos dados para o banco de dados em background.
        Isso pode levar alguns minutos.`,
      });
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }
}

export default new ImportController();
