import ProductService from '../services/ProductService';

class ProductController {
  async index(req, res) {
    try {
      const { page, limit, offset } = req.query;

      const [maxPages, result] = await ProductService.getAll(
        page,
        limit,
        offset,
      );

      return res.json({ page: page || 1, maxPages, result });
    } catch (error) {
      cconsole.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }

  async store(req, res) {
    try {
      const { name } = req.body;

      const product = await ProductService.create({ name });

      return res.json(product);
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }

  async update(req, res) {
    try {
      const { product_id, name } = req.body;

      const product = await ProductService.update({ product_id, name });

      return res.json(product);
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }

  async delete(req, res) {
    try {
      const { product_id } = req.body;

      const product = await ProductService.delete({ product_id });

      return res.json({ message: 'Produto excluído com sucesso!' });
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }
}

export default new ProductController();
