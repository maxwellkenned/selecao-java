import RegionService from '../services/RegionService';

class RegionController {
  async index(req, res) {
    try {
      const { page, limit, offset } = req.query;

      const [maxPages, result] = await RegionService.getAll(
        page,
        limit,
        offset,
      );

      return res.json({ page: page || 1, maxPages, result });
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }

  async store(req, res) {
    try {
      const { name } = req.body;

      const region = await RegionService.create({ name });

      return res.json(region);
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }

  async update(req, res) {
    try {
      const { region_id, name } = req.body;

      const region = await RegionService.update({ region_id, name });

      return res.json(region);
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }

  async delete(req, res) {
    try {
      const { region_id } = req.body;

      const region = await RegionService.delete({ region_id });

      return res.json({ message: 'Região excluído com sucesso!' });
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }
}

export default new RegionController();
