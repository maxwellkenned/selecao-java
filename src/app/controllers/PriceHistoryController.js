import PriceHistoryService from '../services/PriceHistoryService';

class PriceHistoryController {
  async index(req, res) {
    try {
      const { page, limit, offset } = req.query;

      const [maxPages, result] = await PriceHistoryService.getAll(
        page,
        limit,
        offset,
      );

      return res.json({ page: page || 1, maxPages, result });
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }

  async store(req, res) {
    try {
      const priceHistory = await PriceHistoryService.create(req.body);

      return res.json(priceHistory);
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }

  async update(req, res) {
    try {
      const priceHistory = await PriceHistoryService.update(req.body);

      return res.json(priceHistory);
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }

  async delete(req, res) {
    try {
      const { id } = req.body;

      await PriceHistoryService.delete({ id });

      return res.json({ message: 'Histórico excluído com sucesso!' });
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }

  async getAvgPerCityName(req, res) {
    try {
      const { city } = req.query;

      const avgs = await PriceHistoryService.getAvgPerCityName(city);

      return res.json(avgs);
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }

  async getAllPerRegion(req, res) {
    try {
      const { region, page, limit, offset } = req.query;
      const [maxPages, result] = await PriceHistoryService.getAllPerRegion(
        region,
        page,
        limit,
        offset,
      );

      return res.json({ page: page || 1, maxPages, result });
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }

  async getAllPerDistributor(req, res) {
    try {
      const { distributor, page, limit, offset } = req.query;

      const [maxPages, result] = await PriceHistoryService.getAllPerDistributor(
        distributor,
        page,
        limit,
        offset,
      );

      return res.json({ page: page || 1, maxPages, result });
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }

  async getAllPerCollectionDate(req, res) {
    try {
      const { collection_date, page, limit, offset } = req.query;

      const [
        maxPages,
        result,
      ] = await PriceHistoryService.getAllPerCollectionDate(
        collection_date,
        page,
        limit,
        offset,
      );

      return res.json({ page: page || 1, maxPages, result });
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }

  async getAvgPerCity(req, res) {
    try {
      const { page, limit, offset } = req.query;

      const [maxPages, result] = await PriceHistoryService.getAvgPerCity(
        page,
        limit,
        offset,
      );

      return res.json({ page: page || 1, maxPages, result });
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }

  async getAvgPerProvider(req, res) {
    try {
      const { page, limit, offset } = req.query;

      const [maxPages, result] = await PriceHistoryService.getAvgPerProvider(
        page,
        limit,
        offset,
      );

      return res.json({ page: page || 1, maxPages, result });
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }
}

export default new PriceHistoryController();
