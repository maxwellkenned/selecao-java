import ProviderService from '../services/ProviderService';

class ProviderController {
  async index(req, res) {
    try {
      const { page, limit, offset } = req.query;

      const [maxPages, result] = await ProviderService.getAll(
        page,
        limit,
        offset,
      );

      return res.json({ page: page || 1, maxPages, result });
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }

  async store(req, res) {
    try {
      const { name } = req.body;

      const provider = await ProviderService.create({ name });

      return res.json(provider);
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }

  async update(req, res) {
    try {
      const { provider_id, name } = req.body;

      const provider = await ProviderService.update({ provider_id, name });

      return res.json(provider);
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }

  async delete(req, res) {
    try {
      const { provider_id } = req.body;

      const provider = await ProviderService.delete({ provider_id });

      return res.json({ message: 'Fornecedor excluído com sucesso!' });
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }
}

export default new ProviderController();
