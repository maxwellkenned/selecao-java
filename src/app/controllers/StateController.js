import StateService from '../services/StateService';

class StateController {
  async index(req, res) {
    try {
      const { page, limit, offset } = req.query;

      const [maxPages, result] = await StateService.getAll(page, limit, offset);

      return res.json({ page: page || 1, maxPages, result });
    } catch (error) {
      console.log(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }

  async store(req, res) {
    try {
      const { name, region_id } = req.body;

      const state = await StateService.create({ name, region_id });

      return res.json(state);
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }

  async update(req, res) {
    try {
      const { state_id, name } = req.body;

      const state = await StateService.update({ state_id, name });

      return res.json(state);
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }

  async delete(req, res) {
    try {
      const { state_id } = req.body;

      const state = await StateService.delete({ state_id });

      return res.json({ message: 'Estado excluído com sucesso!' });
    } catch (error) {
      console.error(error);
      return res.status(error.status || 500).json({ error: error.message });
    }
  }
}

export default new StateController();
