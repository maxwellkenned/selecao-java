import PriceHistory from '../models/PriceHistory';
import PriceHistoryError from '../errors/PriceHistoryError';
import Query from '../utils/queryUtils';

class PriceHistoryService {
  defaultQuery = `
    SELECT 
      ph.id,
      re.name as region,
      st.name as state,
      ct.name as city,
      dr.name as distributor,
      ph.dist_product_id,
      pr.name as product,
      ph.collection_date,
      ph.purchase_price,
      ph.sale_price,
      ph.unit,
      pv.name as provider
  FROM price_history AS ph
    INNER JOIN products AS pr ON pr.id = ph.product_id
    INNER JOIN distributors AS dr ON dr.id = ph.distributor_id
    INNER JOIN providers AS pv ON pv.id = ph.provider_id
    INNER JOIN cities AS ct ON ct.id = ph.city_id
    INNER JOIN states AS st ON st.id = ct.state_id
    INNER JOIN regions AS re ON re.id = st.region_id 
  `;

  async getAll(page = 1, limit = 100, offset = Query.getOffset(page, limit)) {
    const query = this.defaultQuery;

    const maxPages = await Query.getMaxPages(query, limit);

    const result = await Query.selectPage(query, limit, offset);

    return [maxPages, result];
  }

  async create(priceHistoryRequest) {
    const priceHistoryExists = await PriceHistory.findOne({
      where: { ...priceHistoryRequest },
    });

    if (priceHistoryExists) {
      throw new PriceHistoryError('Histórico de preço já cadastrado!', 400);
    }

    return await PriceHistory.create(priceHistoryRequest);
  }

  async update(priceHistoryRequest) {
    const { id } = priceHistoryRequest;
    const priceHistory = await PriceHistory.findByPk(id);

    if (!priceHistory) {
      throw new PriceHistoryError('Histórico de preço não encontrada', 404);
    }

    return await priceHistory.update(priceHistoryRequest);
  }

  async delete(priceHistoryRequest) {
    const { id } = priceHistoryRequest;

    const priceHistory = await PriceHistory.findByPk(id);

    if (!priceHistory) {
      throw new PriceHistoryError('Histórico de preço não encontrada', 404);
    }

    return await priceHistory.destroy();
  }

  async getAvgPerCityName(cityName) {
    return await Query.select(
      `SELECT c.name AS city, p.name, AVG("sale_price") AS "avg"
        FROM price_history AS ph
        INNER JOIN cities AS c ON ph.city_id = c.id
        INNER JOIN products AS p on ph.product_id = p.id
        WHERE c.name = '${cityName}'
        GROUP BY c.name, p.name`,
    );
  }

  async getAllPerRegion(
    region,
    page = 1,
    limit = 100,
    offset = Query.getOffset(page, limit),
  ) {
    const query = this.defaultQuery + `WHERE re.name = '${region}'`;

    const maxPages = await Query.getMaxPages(query, limit);

    const result = await Query.selectPage(query, limit, offset);

    return [maxPages, result];
  }

  async getAllPerDistributor(
    distributor,
    page = 1,
    limit = 100,
    offset = Query.getOffset(page, limit),
  ) {
    const query = this.defaultQuery + `where dr.name ilike '%${distributor}%'`;

    const maxPages = await Query.getMaxPages(query, limit);
    const result = await Query.selectPage(query, limit, offset);

    return [maxPages, result];
  }

  async getAllPerCollectionDate(
    collectionDate,
    page = 1,
    limit = 100,
    offset = Query.getOffset(page, limit),
  ) {
    const query =
      this.defaultQuery +
      `where CAST(ph.collection_date AS DATE) = '${collectionDate}'`;

    const maxPages = await Query.getMaxPages(query, limit);
    const result = await Query.selectPage(query, limit, offset);

    return [maxPages, result];
  }

  async getAvgPerCity(
    page = 1,
    limit = 100,
    offset = Query.getOffset(page, limit),
  ) {
    const query = `
      SELECT c.name AS city, 
        AVG("sale_price") AS "avg_sale_price", 
        AVG("purchase_price") AS "avg_purchase_price"
      FROM price_history AS ph
      INNER JOIN cities AS c ON ph.city_id = c.id
      GROUP BY c.name
    `;

    const maxPages = await Query.getMaxPages(query, limit);

    const result = await Query.selectPage(query, limit, offset);

    return [maxPages, result];
  }

  async getAvgPerProvider(
    page = 1,
    limit = 100,
    offset = Query.getOffset(page, limit),
  ) {
    const query = `
      SELECT p.name AS provider,
        AVG("sale_price") AS "avg_sale_price", 
        AVG("purchase_price") AS "avg_purchase_price"
      FROM price_history AS ph
      INNER JOIN providers AS p ON ph.provider_id = p.id
      GROUP BY p.name
    `;

    const maxPages = await Query.getMaxPages(query, limit);

    const result = await Query.selectPage(query, limit, offset);

    return [maxPages, result];
  }
}

export default new PriceHistoryService();
