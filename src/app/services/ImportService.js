import { unlinkSync } from 'fs';
import { cargo, eachSeries } from 'async';
import parser from 'csv-parse/lib/sync';
import parse from 'date-fns/parse';

import { eventEmitter } from '../../../bin/server';
import { getFileBuffer } from '../utils/fileHelpers';
import { parserCsvOptions, objCsv } from '../../config/parseCsvOptions';
import ImportError from '../errors/ImportError';

import Import from '../models/Import';
import Region from '../models/Region';
import State from '../models/State';
import City from '../models/City';
import Distributor from '../models/Distributor';
import Product from '../models/Product';
import Provider from '../models/Provider';
import PriceHistory from '../models/PriceHistory';

class ImportService {
  constructor() {
    this.products = {};
    this.distributors = {};
    this.providers = {};
    this.states = {};
    this.cities = {};
  }
  async importCsv(file) {
    try {
      const { buffer, path } = getFileBuffer(file);
      const maxRows = buffer.toString().split('\n').length;
      const numberRows = Math.ceil(maxRows / 9000);

      this._createBulker(buffer);

      setTimeout(async () => {
        await this._populateInfo(numberRows, maxRows);
      }, numberRows * 1000);

      unlinkSync(path);

      return { message: 'Arquivo importado com sucesso!' };
    } catch (error) {
      console.log(error);
      throw new ImportError();
    }
  }

  async _createBulker(buffer) {
    await Import.destroy({ truncate: true, restartIdentity: true });

    const inserter = cargo(async (data, cb) => {
      await Import.bulkCreate(data);
    }, 2000);

    const records = parser(buffer, parserCsvOptions);
    eachSeries(records, async row => {
      if (!row) return;

      const [
        region,
        state,
        city,
        distributor,
        dist_product_id,
        product,
        collection_date,
        purchase_price,
        sale_price,
        unit,
        provider,
      ] = this._validRow(row);

      inserter.push({
        region,
        state,
        city,
        distributor,
        dist_product_id,
        product,
        collection_date,
        purchase_price,
        sale_price,
        unit,
        provider,
      });
    });
  }

  _validRow(row) {
    const validRow = [];
    if (row.length > 11) {
      for (let i = 0; i <= row.length; i++) {
        let data = row[i];

        if (objCsv[i] && objCsv[i].name === 'distributor') {
          let rowPart = row.slice(i);
          const result = [];

          for (let j = 0; j < rowPart.length; j++) {
            const number = Number.parseInt(rowPart[j]);
            if (!!number && number > 999) break;
            result.push(rowPart[j]);
          }

          if (result.length) {
            data = result.join(' ');
            i += result.length - 1;
          }
        }

        validRow.push(data);
      }
    }

    return validRow.length ? validRow : row;
  }

  async _populateInfo(numberRows, maxRows) {
    await this._pupulateRegions();

    setTimeout(
      async () => await this._pupulateStates(),
      Math.max(1000, numberRows * 20),
    );
    setTimeout(
      async () => await this._pupulateProducts(),
      Math.max(1500, numberRows * 40),
    );
    setTimeout(
      async () => await this._pupulateCities(),
      Math.max(2000, numberRows * 80),
    );
    setTimeout(
      async () => await this._pupulateProviders(),
      Math.max(4000, numberRows * 200),
    );
    setTimeout(
      async () => await this._pupulateDistributors(),
      Math.max(20000, numberRows * 600),
    );
    setTimeout(
      async () => await this._populatePriceHistories(maxRows),
      Math.max(30000, numberRows * 800),
    );
  }

  async _pupulateRegions() {
    const imports = await Import.findAll({
      attributes: ['region'],
      group: ['region'],
    });

    eachSeries(imports, async data => {
      await Region.findOrCreate({ where: { name: data.region } });
    });
  }

  async _pupulateStates() {
    const imports = await Import.findAll({
      attributes: ['region', 'state'],
      group: ['region', 'state'],
    });

    let region = null;

    eachSeries(imports, async data => {
      if (!region || region.name !== data.region) {
        region = await Region.findOne({ where: { name: data.region } });
      }
      if (region['id']) {
        await State.findOrCreate({
          where: { name: data.state, region_id: region.id },
        });
      }
    });
  }

  async _pupulateCities() {
    const imports = await Import.findAll({
      attributes: ['region', 'state', 'city'],
      group: ['region', 'state', 'city'],
    });

    let state = null;

    imports.forEach(async data => {
      if (!state || state.name !== data.state) {
        state = await State.findOne({ where: { name: data.state } });
      }

      if (state && state.id) {
        await City.findCreateFind({
          where: { name: data.city, state_id: state.id },
        });
      }
    });
  }

  async _pupulateDistributors() {
    const imports = await Import.findAll({
      attributes: ['region', 'state', 'city', 'distributor'],
      group: ['region', 'state', 'city', 'distributor'],
    });

    let city = null;

    imports.forEach(async data => {
      if (!city || city.name !== data.city) {
        city = await City.findOne({ where: { name: data.city } });
      }
      if (city.id) {
        await Distributor.findOrCreate({
          where: { name: data.distributor, city_id: city.id },
        });
      }
    });
  }

  async _pupulateProducts() {
    const imports = await Import.findAll({
      attributes: ['product'],
      group: ['product'],
    });

    eachSeries(imports, async data => {
      await Product.findOrCreate({ where: { name: data.product } });
    });
  }

  async _pupulateProviders() {
    const imports = await Import.findAll({
      attributes: ['provider'],
      group: ['provider'],
    });

    eachSeries(imports, async data => {
      await Provider.findOrCreate({ where: { name: data.provider } });
    });
  }

  async _populatePriceHistories() {
    const imports = await Import.findAll();

    const inserter = cargo(async (data, cb) => {
      await PriceHistory.bulkCreate(data, { ignoreDuplicates: true });
    }, 2000);

    eachSeries(imports, async data => {
      if (!Object.entries(this.states).length) {
        const stateAll = await State.findAll({
          attributes: ['id', 'name'],
          lock: true,
        });

        stateAll.forEach(state => {
          this.states[state.name] = state.id;
        });
      }

      if (!this.products[data.product]) {
        const product = await Product.findOne({
          attributes: ['id'],
          where: { name: data.product },
        });

        if (product) this.products[data.product] = product.id;
      }

      if (!this.cities[data.city]) {
        const city = await City.findOne({
          attributes: ['id'],
          where: { name: data.city, state_id: this.states[data.state] },
        });

        if (city) this.cities[data.city] = city.id;
      }

      if (!this.distributors[data.distributor]) {
        const distributor = await Distributor.findOne({
          attributes: ['id'],
          where: { name: data.distributor, city_id: this.cities[data.city] },
        });

        if (distributor) this.distributors[data.distributor] = distributor.id;
      }

      if (!this.providers[data.provider]) {
        const provider = await Provider.findOne({
          attributes: ['id'],
          where: { name: data.provider },
        });

        if (provider) this.providers[data.provider] = provider.id;
      }

      if (
        this.products[data.product] &&
        this.distributors[data.distributor] &&
        this.providers[data.provider] &&
        this.cities[data.city]
      ) {
        inserter.push({
          dist_product_id: data.dist_product_id,
          collection_date: parse(
            data.collection_date,
            'dd/MM/yyyy',
            new Date(),
          ),
          purchase_price: data.purchase_price
            ? data.purchase_price.replace(',', '.')
            : 0.0,
          sale_price: data.sale_price ? data.sale_price.replace(',', '.') : 0.0,
          unit: data.unit,
          product_id: this.products[data.product],
          distributor_id: this.distributors[data.distributor],
          provider_id: this.providers[data.provider],
          city_id: this.cities[data.city],
        });
      }
    }).then(() => {
      eventEmitter.emit('upload', 'finalizado');
    });
  }
}

export default new ImportService();
