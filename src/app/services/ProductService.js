import Product from '../models/Product';
import ProductError from '../errors/ProductError';
import Query from '../utils/queryUtils';

class ProductService {
  async getAll(page = 1, limit = 100, offset = Query.getOffset(page, limit)) {
    const count = await Product.count({ limit, offset });
    const result = await Product.findAll({ limit, offset });
    const maxPages = Query.getPages(count, limit);

    return [maxPages, result];
  }

  async create(productRequest) {
    const { name } = productRequest;

    const productExists = await Product.findOne({ where: { name } });

    if (productExists) {
      throw new ProductError('Produto já cadastrado!', 400);
    }

    return await Product.create({ name });
  }

  async update(productRequest) {
    const { product_id, name } = productRequest;

    const product = await Product.findByPk(product_id);

    if (!product) {
      throw new ProductError('Produto não encontrada', 404);
    }

    return await product.update({ name });
  }

  async delete(productRequest) {
    const { product_id } = productRequest;

    const product = await Product.findByPk(product_id);

    if (!product) {
      throw new ProductError('Produto não encontrada', 404);
    }

    return await product.destroy();
  }
}

export default new ProductService();
