import Provider from '../models/Provider';
import ProviderError from '../errors/ProviderError';
import Query from '../utils/queryUtils';

class ProviderService {
  async getAll(page = 1, limit = 100, offset = Query.getOffset(page, limit)) {
    const count = await Provider.count({ limit, offset });
    const result = await Provider.findAll({ limit, offset });
    const maxPages = Query.getPages(count, limit);

    return [maxPages, result];
  }

  async create(providerRequest) {
    const { name } = providerRequest;

    const providerExists = await Provider.findOne({ where: { name } });

    if (providerExists) {
      throw new ProviderError('Fornecedor já cadastrado!', 400);
    }

    return await Provider.create({ name });
  }

  async update(providerRequest) {
    const { provider_id, name } = providerRequest;

    const provider = await Provider.findByPk(provider_id);

    if (!provider) {
      throw new ProviderError('Fornecedor não encontrada', 404);
    }

    return await provider.update({ name });
  }

  async delete(providerRequest) {
    const { provider_id } = providerRequest;

    const provider = await Provider.findByPk(provider_id);

    if (!provider) {
      throw new ProviderError('Fornecedor não encontrada', 404);
    }

    return await provider.destroy();
  }
}

export default new ProviderService();
