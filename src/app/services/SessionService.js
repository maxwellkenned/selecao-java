import jwt from 'jsonwebtoken';

import User from '../models/User';
import authConfig from '../../config/auth';
import SessionValidator from '../validators/SessionValidator';

class SessionService {
  async createSession(email, password) {
    await SessionValidator.createValidation({ email, password });

    const user = await User.findOne({ where: { email } });

    if (!user) {
      return { error: 'Usuário não encontrado!' };
    }

    if (!(await user.checkPassword(password))) {
      return { error: 'Senha inválida!' };
    }

    const { id, name } = user;

    return {
      user: { id, name, email },
      token: jwt.sign({ id }, authConfig.secret, {
        expiresIn: authConfig.expiresIn,
      }),
    };
  }
}

export default new SessionService();
