import City from '../models/City';
import CityError from '../errors/CityError';
import Query from '../utils/queryUtils';

class CityService {
  async getAll(page = 1, limit = 100, offset = Query.getOffset(page, limit)) {
    const count = await City.count({ limit, offset });
    const result = await City.findAll({ limit, offset });
    const maxPages = Query.getPages(count, limit);

    return [maxPages, result];
  }

  async create(cityRequest) {
    const { name, state_id } = cityRequest;

    const cityExists = await City.findOne({
      where: { name, state_id },
    });

    if (cityExists) {
      throw new CityError('Cidade já cadastrado!', 400);
    }

    return await City.create({ name, state_id });
  }

  async update(cityRequest) {
    const { city_id, name } = cityRequest;

    const city = await City.findByPk(city_id);

    if (!city) {
      throw new CityError('Cidade não encontrada', 404);
    }

    return await city.update({ name });
  }

  async delete(cityRequest) {
    const { city_id } = cityRequest;

    const city = await City.findByPk(city_id);

    if (!city) {
      throw new CityError('Cidade não encontrada', 404);
    }

    return await city.destroy();
  }
}

export default new CityService();
