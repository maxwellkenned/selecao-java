import Distributor from '../models/Distributor';
import DistributorError from '../errors/DistributorError';
import Query from '../utils/queryUtils';

class DistributorService {
  async getAll(page = 1, limit = 100, offset = Query.getOffset(page, limit)) {
    const count = await Distributor.count({ limit, offset });
    const result = await Distributor.findAll({ limit, offset });
    const maxPages = Query.getPages(count, limit);

    return [maxPages, result];
  }

  async create(distributorRequest) {
    const { name, city_id } = distributorRequest;

    const distributorExists = await Distributor.findOne({
      where: { name, city_id },
    });

    if (distributorExists) {
      throw new DistributorError('Distribuidor já cadastrado!', 400);
    }

    return await Distributor.create({ name, city_id });
  }

  async update(distributorRequest) {
    const { distributor_id, name } = distributorRequest;

    const distributor = await Distributor.findByPk(distributor_id);

    if (!distributor) {
      throw new DistributorError('Distribuidor não encontrada', 404);
    }

    return await distributor.update({ name });
  }

  async delete(distributorRequest) {
    const { distributor_id } = distributorRequest;

    const distributor = await Distributor.findByPk(distributor_id);

    if (!distributor) {
      throw new DistributorError('Distribuidor não encontrada', 404);
    }

    return await distributor.destroy();
  }
}

export default new DistributorService();
