import User from '../models/User';
import UserValidator from '../validators/UserValidator';
import UserError from '../errors/UserError';
import Query from '../utils/queryUtils';

class UserService {
  async getAll(page = 1, limit = 100, offset = Query.getOffset(page, limit)) {
    const count = await User.count({ limit, offset });
    const result = await User.findAll({ limit, offset });
    const maxPages = Query.getPages(count, limit);

    return [maxPages, result];
  }

  async create(userRequest) {
    const { name, email, password } = userRequest;

    await UserValidator.createValidation({ name, email, password });

    const userExists = await User.findOne({ where: { email } });

    if (userExists) {
      throw new UserError('E-mail já cadastrado!', 400);
    }

    return await User.create({ name, email, password });
  }

  async update(userRequest) {
    const {
      userId,
      name,
      email,
      password,
      oldPassword,
      confirmPassword,
    } = userRequest;

    await UserValidator.updateValidation({
      name,
      email,
      password,
      oldPassword,
      confirmPassword,
    });

    const user = await User.findByPk(userId);

    if (!user) {
      throw new UserError('Usuário não encontrada', 404);
    }

    if (email !== user.email) {
      const userExists = await User.findOne({ where: { email } });

      if (userExists) {
        throw new UserError('E-mail já cadastrado!', 400);
      }
    }

    if (oldPassword && !(await user.checkPassword(oldPassword))) {
      throw new UserError('Senha inválida!', 401);
    }

    return await user.update({ name, email, password });
  }

  async delete(userRequest) {
    const { id } = userRequest;

    const user = await User.findByPk(id);

    if (!user) {
      throw new UserError('Usuário não encontrada', 404);
    }

    return await user.destroy();
  }
}

export default new UserService();
