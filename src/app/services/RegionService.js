import Region from '../models/Region';
import RegionError from '../errors/RegionError';
import Query from '../utils/queryUtils';

class RegionService {
  async getAll(page = 1, limit = 100, offset = Query.getOffset(page, limit)) {
    const count = await Region.count({ limit, offset });
    const result = await Region.findAll({ limit, offset });
    const maxPages = Query.getPages(count, limit);

    return [maxPages, result];
  }

  async create(regionRequest) {
    const { name } = regionRequest;

    const regionExists = await Region.findOne({ where: { name } });

    if (regionExists) {
      throw new RegionError('Região já cadastrado!', 400);
    }

    return await Region.create({ name });
  }

  async update(regionRequest) {
    const { region_id, name } = regionRequest;

    const region = await Region.findByPk(region_id);

    if (!region) {
      throw new RegionError('Região não encontrada', 404);
    }

    return await region.update({ name });
  }

  async delete(regionRequest) {
    const { region_id } = regionRequest;

    const region = await Region.findByPk(region_id);

    if (!region) {
      throw new RegionError('Região não encontrada', 404);
    }

    return await region.destroy();
  }
}

export default new RegionService();
