import State from '../models/State';
import StateError from '../errors/StateError';
import Query from '../utils/queryUtils';

class StateService {
  async getAll(page = 1, limit = 100, offset = Query.getOffset(page, limit)) {
    const count = await State.count({ limit, offset });
    const result = await State.findAll({ limit, offset });
    const maxPages = Query.getPages(count, limit);

    return [maxPages, result];
  }

  async create(stateRequest) {
    const { name, region_id } = stateRequest;

    const stateExists = await State.findOne({
      where: { name, region_id },
    });

    if (stateExists) {
      throw new StateError('Estado já cadastrado!', 400);
    }

    return await State.create({ name, region_id });
  }

  async update(stateRequest) {
    const { state_id, name } = stateRequest;

    const state = await State.findByPk(state_id);

    if (!state) {
      throw new StateError('Estado não encontrada', 404);
    }

    return await state.update({ name });
  }

  async delete(stateRequest) {
    const { state_id } = stateRequest;

    const state = await State.findByPk(state_id);

    if (!state) {
      throw new StateError('Estado não encontrada', 404);
    }

    return await state.destroy();
  }
}

export default new StateService();
