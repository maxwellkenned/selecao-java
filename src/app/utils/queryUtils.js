import connection from '../../config/connection';

class Query {
  async getMaxPages(query, limit) {
    const [count] = await connection.query(
      `SELECT count(*) FROM (${query}) AS result`,
    );

    return this.getPages(count[0].count, limit);
  }

  async selectPage(query, limit, offset) {
    const [result] = await connection.query(
      `${query} ORDER BY 1 LIMIT ${limit} OFFSET ${offset}`,
    );

    return result;
  }

  async select(query) {
    const [result] = await connection.query(query);

    return result;
  }

  getOffset(page, limit) {
    let offset = 0;

    if (page > 1) {
      offset = limit * (page - 1);
    }

    return offset;
  }

  getPages(count, limit) {
    return Math.ceil(count / limit) || 0;
  }
}

export default new Query();
