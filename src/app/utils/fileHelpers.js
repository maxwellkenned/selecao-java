import { readFileSync } from 'fs';
import { resolve } from 'path';

function getFileBuffer(file) {
  const path = resolve(
    __dirname,
    '..',
    '..',
    '..',
    'tmp',
    'uploads',
    file.filename,
  );

  return { buffer: readFileSync(path), path };
}

export { getFileBuffer };
