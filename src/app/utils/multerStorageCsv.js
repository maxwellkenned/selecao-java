import multer from 'multer';
import { extname } from 'path';

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'tmp/uploads/');
  },
  filename: (req, file, cb) => {
    cb(
      null,
      `${file.fieldname}-${req.userId}-${Date.now()}${extname(
        file.originalname,
      )}`,
    );
  },
});

function fileFilter(req, file, cb) {
  if (!file || file.mimetype !== 'text/csv') {
    return cb(null, false);
  }

  cb(null, true);
}

export default multer({ storage, fileFilter });
