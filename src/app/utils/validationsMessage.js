export default {
  required: '${path} é obrigatório',
  email: '${path} deve ser um e-mail válido',
  min: '${path} deve ter pelo menos ${min} caracteres',
  max: '${path} deve ter máximo ${max} caracteres'
};
