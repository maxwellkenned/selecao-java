import express from 'express';
import cookieParser from 'cookie-parser';
import cors from 'cors';

import routes from './routes';

import './database';

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());

app.use(routes);

export default app;
