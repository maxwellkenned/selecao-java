import SocketIO from 'socket.io';

export default class SocketIo {
  io;

  constructor(server) {
    this.io = SocketIO(server, { origins: '*:*' });
  }

  upload() {
    this.io.emit('upload', 'finalizado');
  }
}
