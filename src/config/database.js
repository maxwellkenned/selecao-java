module.exports = {
  username: 'docker',
  password: 'docker',
  database: 'docker',
  host: 'db',
  port: 5432,
  dialect: 'postgres',
  timezone: 'America/Sao_Paulo',
  logging: false,
  define: {
    timestamps: true,
    underscored: true,
  },
  pool: {
    max: 6,
    idle: 30000,
  },
};
