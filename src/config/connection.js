import Sequelize from 'sequelize';

import dbConfig from './database';

export default new Sequelize(dbConfig);
