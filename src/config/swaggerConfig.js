export default {
  swagger: '2.0',
  info: {
    version: '1.0.0',
    title: 'Maxwell Kenned API',
    description: 'Uma simples api para um processo de seleção',
    license: {
      name: 'MIT',
      url: 'https://opensource.org/licenses/MIT',
    },
  },
  schemes: ['http'],
  host: 'http://localhost:9000',
  basePath: '/',
  tags: [
    {
      name: 'Users',
      description: 'Rotas de usuário',
    },
    {
      name: 'Sessions',
      description: 'Rotas de sessão',
    },
    {
      name: 'Import',
      description: 'Rota de import',
    },
    {
      name: 'PriceHistory',
      description: 'Rota de historico de preço',
    },
    {
      name: 'Cities',
      description: 'Rotas de cidade',
    },
    {
      name: 'Distributors',
      description: 'Rotas de distribuidores',
    },
    {
      name: 'Products',
      description: 'Rotas de produto',
    },
    {
      name: 'Providers',
      description: 'Rotas de fornecedor',
    },
    {
      name: 'Regions',
      description: 'Rotas de região',
    },
    {
      name: 'States',
      description: 'Rotas de estado',
    },
  ],
  paths: {
    '/sessions': {
      post: {
        tags: ['Sessions'],
        summary: 'Cria uma nova sessão na API',
        parameters: [
          {
            name: 'email',
            in: 'body',
            required: true,
            description: 'E-mail do usuário',
            type: 'string',
          },
          {
            name: 'password',
            in: 'body',
            required: true,
            description: 'Senha do usuário',
            type: 'string',
          },
        ],
      },
    },
    '/import': {
      post: {
        tags: ['Import'],
        summary: 'Importar arquivo csv',
        consumes: ['multipart/form-data'],
        parameters: [
          {
            name: 'file',
            in: 'formData',
            type: 'file',
            description: 'Arquivo csv com histórico de preço',
            'x-mimetype': 'text/csv',
          },
        ],
      },
    },
    '/users': {
      get: {
        tags: ['Users'],
        summary: 'Obtém todos os usuários',
        parameters: [
          {
            name: 'Authorization',
            in: 'header',
            required: true,
            description: 'Token da sessão',
          },
          {
            name: 'page',
            in: 'query',
            required: false,
            description: 'Número da página de usuários',
            type: 'int',
          },
          {
            name: 'limit',
            in: 'query',
            required: false,
            description: 'Limite de dados por requisição',
            type: 'int',
          },
          {
            name: 'offset',
            in: 'query',
            required: false,
            description: 'Pegar os dados à patir desse numero',
            type: 'int',
          },
        ],
      },
      post: {
        tags: ['Users'],
        summary: 'Cadastrar um novo usuário',
        parameters: [
          {
            name: 'name',
            in: 'body',
            required: true,
            description: 'Nome do usuário',
            type: 'string',
          },
          {
            name: 'email',
            in: 'body',
            required: true,
            description: 'E-mail do usuário',
            type: 'string',
          },
          {
            name: 'password',
            in: 'body',
            required: true,
            description: 'Senha do usuário',
            type: 'string',
          },
        ],
      },
      put: {
        tags: ['Users'],
        summary: 'Atualiza o usuário logado',
        parameters: [
          {
            name: 'Authorization',
            in: 'header',
            required: true,
            description: 'Token da sessão',
          },
          {
            name: 'name',
            in: 'body',
            required: true,
            description: 'Nome do usuário',
            type: 'string',
          },
          {
            name: 'email',
            in: 'body',
            required: true,
            description: 'E-mail do usuário',
            type: 'string',
          },
          {
            name: 'oldPassword',
            in: 'body',
            required: true,
            description: 'Senha atual do usuário',
            type: 'string',
          },
          {
            name: 'password',
            in: 'body',
            required: true,
            description: 'Nova senha do usuário',
            type: 'string',
          },
          {
            name: 'confirmPassword',
            in: 'body',
            required: true,
            description: 'Confirmar senha nova do usuário',
            type: 'string',
          },
        ],
      },
      delete: {
        tags: ['Users'],
        summary: 'Deleta o usuário logado',
        parameters: [
          {
            name: 'Authorization',
            in: 'header',
            required: true,
            description: 'Token da sessão',
          },
        ],
      },
    },
    '/cities': {
      get: {
        tags: ['Cities'],
        summary: 'Obtém todas as cidades',
        parameters: [
          {
            name: 'Authorization',
            in: 'header',
            required: true,
            description: 'Token da sessão',
          },
          {
            name: 'page',
            in: 'query',
            required: false,
            description: 'Número da página',
            type: 'int',
          },
          {
            name: 'limit',
            in: 'query',
            required: false,
            description: 'Limite de dados por requisição',
            type: 'int',
          },
          {
            name: 'offset',
            in: 'query',
            required: false,
            description: 'Pegar os dados à patir desse número',
            type: 'int',
          },
        ],
      },
      post: {
        tags: ['Cities'],
        summary: 'Cadastrar uma nova cidade',
        parameters: [
          {
            name: 'Authorization',
            in: 'header',
            required: true,
            description: 'Token da sessão',
          },
          {
            name: 'name',
            in: 'body',
            required: true,
            description: 'Nome da cidade',
            type: 'string',
          },
          {
            name: 'state_id',
            in: 'body',
            required: true,
            description: 'Id do estado',
            type: 'int',
          },
        ],
      },
      put: {
        tags: ['Cities'],
        summary: 'Atualiza uma cidade',
        parameters: [
          {
            name: 'Authorization',
            in: 'header',
            required: true,
            description: 'Token da sessão',
          },
          {
            name: 'name',
            in: 'body',
            required: true,
            description: 'Nome da cidade',
            type: 'string',
          },
          {
            name: 'city_id',
            in: 'body',
            required: true,
            description: 'Id da cidade',
            type: 'int',
          },
        ],
      },
      delete: {
        tags: ['Cities'],
        summary: 'Deleta uma cidade',
        parameters: [
          {
            name: 'city_id',
            in: 'body',
            required: true,
            description: 'Id da cidade',
            type: 'int',
          },
        ],
      },
    },
    '/distributors': {
      get: {
        tags: ['Distributors'],
        summary: 'Obtém todos os distribuidores',
        parameters: [
          {
            name: 'Authorization',
            in: 'header',
            required: true,
            description: 'Token da sessão',
          },
          {
            name: 'page',
            in: 'query',
            required: false,
            description: 'Número da página',
            type: 'int',
          },
          {
            name: 'limit',
            in: 'query',
            required: false,
            description: 'Limite de dados por requisição',
            type: 'int',
          },
          {
            name: 'offset',
            in: 'query',
            required: false,
            description: 'Pegar os dados à patir desse número',
            type: 'int',
          },
        ],
      },
      post: {
        tags: ['Distributors'],
        summary: 'Cadastra um novo distribuidor',
        parameters: [
          {
            name: 'Authorization',
            in: 'header',
            required: true,
            description: 'Token da sessão',
          },
          {
            name: 'name',
            in: 'body',
            required: true,
            description: 'Nome do distribuidor',
            type: 'string',
          },
          {
            name: 'city_id',
            in: 'body',
            required: true,
            description: 'Id da cidade',
            type: 'int',
          },
        ],
      },
      put: {
        tags: ['Distributors'],
        summary: 'Atualiza um distribuidor',
        parameters: [
          {
            name: 'Authorization',
            in: 'header',
            required: true,
            description: 'Token da sessão',
          },
          {
            name: 'name',
            in: 'body',
            required: true,
            description: 'Nome do distribuidor',
            type: 'string',
          },
          {
            name: 'distributor_id',
            in: 'body',
            required: true,
            description: 'Id do distribuidor',
            type: 'int',
          },
        ],
      },
      delete: {
        tags: ['Distributors'],
        summary: 'Deleta um distribuidor',
        parameters: [
          {
            name: 'distributor_id',
            in: 'body',
            required: true,
            description: 'Id do distribuidor',
            type: 'int',
          },
        ],
      },
    },
    '/products': {
      get: {
        tags: ['Products'],
        summary: 'Obtém todos os produtos',
        parameters: [
          {
            name: 'Authorization',
            in: 'header',
            required: true,
            description: 'Token da sessão',
          },
          {
            name: 'page',
            in: 'query',
            required: false,
            description: 'Número da página',
            type: 'int',
          },
          {
            name: 'limit',
            in: 'query',
            required: false,
            description: 'Limite de dados por requisição',
            type: 'int',
          },
          {
            name: 'offset',
            in: 'query',
            required: false,
            description: 'Pegar os dados à patir desse número',
            type: 'int',
          },
        ],
      },
      post: {
        tags: ['Products'],
        summary: 'Cadastra um novo produto',
        parameters: [
          {
            name: 'Authorization',
            in: 'header',
            required: true,
            description: 'Token da sessão',
          },
          {
            name: 'name',
            in: 'body',
            required: true,
            description: 'Nome do produto',
            type: 'string',
          },
        ],
      },
      put: {
        tags: ['Products'],
        summary: 'Atualiza um produto',
        parameters: [
          {
            name: 'Authorization',
            in: 'header',
            required: true,
            description: 'Token da sessão',
          },
          {
            name: 'name',
            in: 'body',
            required: true,
            description: 'Nome do produto',
            type: 'string',
          },
          {
            name: 'product_id',
            in: 'body',
            required: true,
            description: 'Id do produto',
            type: 'int',
          },
        ],
      },
      delete: {
        tags: ['Products'],
        summary: 'Deleta um produto',
        parameters: [
          {
            name: 'product_id',
            in: 'body',
            required: true,
            description: 'Id do produto',
            type: 'int',
          },
        ],
      },
    },
    '/providers': {
      get: {
        tags: ['Providers'],
        summary: 'Obtém todos os fornecedores',
        parameters: [
          {
            name: 'Authorization',
            in: 'header',
            required: true,
            description: 'Token da sessão',
          },
          {
            name: 'page',
            in: 'query',
            required: false,
            description: 'Número da página',
            type: 'int',
          },
          {
            name: 'limit',
            in: 'query',
            required: false,
            description: 'Limite de dados por requisição',
            type: 'int',
          },
          {
            name: 'offset',
            in: 'query',
            required: false,
            description: 'Pegar os dados à patir desse número',
            type: 'int',
          },
        ],
      },
      post: {
        tags: ['Providers'],
        summary: 'Cadastra um novo fornecedor',
        parameters: [
          {
            name: 'Authorization',
            in: 'header',
            required: true,
            description: 'Token da sessão',
          },
          {
            name: 'name',
            in: 'body',
            required: true,
            description: 'Nome do fornecedor',
            type: 'string',
          },
        ],
      },
      put: {
        tags: ['Providers'],
        summary: 'Atualiza um fornecedor',
        parameters: [
          {
            name: 'Authorization',
            in: 'header',
            required: true,
            description: 'Token da sessão',
          },
          {
            name: 'name',
            in: 'body',
            required: true,
            description: 'Nome do fornecedor',
            type: 'string',
          },
          {
            name: 'provider_id',
            in: 'body',
            required: true,
            description: 'Id do fornecedor',
            type: 'int',
          },
        ],
      },
      delete: {
        tags: ['Providers'],
        summary: 'Deleta um fornecedor',
        parameters: [
          {
            name: 'provider_id',
            in: 'body',
            required: true,
            description: 'Id do fornecedor',
            type: 'int',
          },
        ],
      },
    },
    '/regions': {
      get: {
        tags: ['Regions'],
        summary: 'Obtém todos os regiões',
        parameters: [
          {
            name: 'Authorization',
            in: 'header',
            required: true,
            description: 'Token da sessão',
          },
          {
            name: 'page',
            in: 'query',
            required: false,
            description: 'Número da página',
            type: 'int',
          },
          {
            name: 'limit',
            in: 'query',
            required: false,
            description: 'Limite de dados por requisição',
            type: 'int',
          },
          {
            name: 'offset',
            in: 'query',
            required: false,
            description: 'Pegar os dados à patir desse número',
            type: 'int',
          },
        ],
      },
      post: {
        tags: ['Regions'],
        summary: 'Cadastra uma nova região',
        parameters: [
          {
            name: 'Authorization',
            in: 'header',
            required: true,
            description: 'Token da sessão',
          },
          {
            name: 'name',
            in: 'body',
            required: true,
            description: 'Nome da região',
            type: 'string',
          },
        ],
      },
      put: {
        tags: ['Regions'],
        summary: 'Atualiza uma região',
        parameters: [
          {
            name: 'Authorization',
            in: 'header',
            required: true,
            description: 'Token da sessão',
          },
          {
            name: 'name',
            in: 'body',
            required: true,
            description: 'Sigla da região',
            type: 'string',
          },
          {
            name: 'region_id',
            in: 'body',
            required: true,
            description: 'Id do região',
            type: 'int',
          },
        ],
      },
      delete: {
        tags: ['Regions'],
        summary: 'Deleta um região',
        parameters: [
          {
            name: 'region_id',
            in: 'body',
            required: true,
            description: 'Id do região',
            type: 'int',
          },
        ],
      },
    },
    '/states': {
      get: {
        tags: ['States'],
        summary: 'Obtém todos os estados',
        parameters: [
          {
            name: 'Authorization',
            in: 'header',
            required: true,
            description: 'Token da sessão',
          },
          {
            name: 'page',
            in: 'query',
            required: false,
            description: 'Número da página',
            type: 'int',
          },
          {
            name: 'limit',
            in: 'query',
            required: false,
            description: 'Limite de dados por requisição',
            type: 'int',
          },
          {
            name: 'offset',
            in: 'query',
            required: false,
            description: 'Pegar os dados à patir desse número',
            type: 'int',
          },
        ],
      },
      post: {
        tags: ['States'],
        summary: 'Cadastra um novo estado',
        parameters: [
          {
            name: 'Authorization',
            in: 'header',
            required: true,
            description: 'Token da sessão',
          },
          {
            name: 'name',
            in: 'body',
            required: true,
            description: 'Sigla do estado',
            type: 'string',
          },
          {
            name: 'region_id',
            in: 'body',
            required: true,
            description: 'Id da região',
            type: 'int',
          },
        ],
      },
      put: {
        tags: ['States'],
        summary: 'Atualiza um estado',
        parameters: [
          {
            name: 'Authorization',
            in: 'header',
            required: true,
            description: 'Token da sessão',
          },
          {
            name: 'name',
            in: 'body',
            required: true,
            description: 'Sigla do estado',
            type: 'string',
          },
          {
            name: 'state_id',
            in: 'body',
            required: true,
            description: 'Id do estado',
            type: 'int',
          },
        ],
      },
      delete: {
        tags: ['States'],
        summary: 'Deleta um estado',
        parameters: [
          {
            name: 'state_id',
            in: 'body',
            required: true,
            description: 'Id do estado',
            type: 'int',
          },
        ],
      },
    },
    '/pricehistories': {
      get: {
        tags: ['PriceHistory'],
        summary: 'Obtém todos os históricos de preços',
        parameters: [
          {
            name: 'Authorization',
            in: 'header',
            required: true,
            description: 'Token da sessão',
          },
          {
            name: 'page',
            in: 'query',
            required: false,
            description: 'Número da página',
            type: 'int',
          },
          {
            name: 'limit',
            in: 'query',
            required: false,
            description: 'Limite de dados por requisição',
            type: 'int',
          },
          {
            name: 'offset',
            in: 'query',
            required: false,
            description: 'Pegar os dados à patir desse número',
            type: 'int',
          },
        ],
      },
      post: {
        tags: ['PriceHistory'],
        summary: 'Cadastra um novo histórico de preço',
        parameters: [
          {
            name: 'Authorization',
            in: 'header',
            required: true,
            description: 'Token da sessão',
          },
          {
            name: 'dist_product_id',
            in: 'body',
            required: false,
            description: 'Id do produto no distribuidor',
            type: 'int',
          },
          {
            name: 'collection_date',
            in: 'body',
            required: true,
            description: 'Data da coleta',
            type: 'date',
          },
          {
            name: 'purchase_price',
            in: 'body',
            required: true,
            description: 'Preço de compra',
            type: 'double',
          },
          {
            name: 'sale_price',
            in: 'body',
            required: true,
            description: 'Preço de venda',
            type: 'double',
          },
          {
            name: 'unit',
            in: 'body',
            required: true,
            description: 'unidade de medida',
            type: 'string',
          },
          {
            name: 'product_id',
            in: 'body',
            required: true,
            description: 'Id do produto',
            type: 'int',
          },
          {
            name: 'distributor_id',
            in: 'body',
            required: true,
            description: 'Id do distribuidor',
            type: 'int',
          },
          {
            name: 'provider_id',
            in: 'body',
            required: true,
            description: 'Id do fornecedor',
            type: 'int',
          },
          {
            name: 'city_id',
            in: 'body',
            required: true,
            description: 'Id da cidade',
            type: 'int',
          },
        ],
      },
      put: {
        tags: ['PriceHistory'],
        summary: 'Atualiza um histórico de preço',
        parameters: [
          {
            name: 'Authorization',
            in: 'header',
            required: true,
            description: 'Token da sessão',
          },
          {
            name: 'dist_product_id',
            in: 'body',
            required: false,
            description: 'Id do produto no distribuidor',
            type: 'int',
          },
          {
            name: 'collection_date',
            in: 'body',
            required: false,
            description: 'Data da coleta',
            type: 'date',
          },
          {
            name: 'purchase_price',
            in: 'body',
            required: false,
            description: 'Preço de compra',
            type: 'double',
          },
          {
            name: 'sale_price',
            in: 'body',
            required: false,
            description: 'Preço de venda',
            type: 'double',
          },
          {
            name: 'unit',
            in: 'body',
            required: false,
            description: 'unidade de medida',
            type: 'string',
          },
          {
            name: 'product_id',
            in: 'body',
            required: false,
            description: 'Id do produto',
            type: 'int',
          },
          {
            name: 'distributor_id',
            in: 'body',
            required: false,
            description: 'Id do distribuidor',
            type: 'int',
          },
          {
            name: 'provider_id',
            in: 'body',
            required: false,
            description: 'Id do fornecedor',
            type: 'int',
          },
          {
            name: 'city_id',
            in: 'body',
            required: false,
            description: 'Id da cidade',
            type: 'int',
          },
        ],
      },
      delete: {
        tags: ['PriceHistory'],
        summary: 'Deleta um histórico de preço',
        parameters: [
          {
            name: 'id',
            in: 'body',
            required: true,
            description: 'Id do histórico de preço',
            type: 'int',
          },
        ],
      },
    },
    '/pricehistories/avg/city': {
      get: {
        tags: ['PriceHistory'],
        summary:
          'Retorna a média de preço de combustível com base no nome da cidade',
        parameters: [
          {
            name: 'Authorization',
            in: 'header',
            required: true,
            description: 'Token da sessão',
          },
          {
            name: 'city',
            in: 'query',
            required: true,
            description: 'Nome da cidade',
            type: 'string',
          },
        ],
      },
    },
    '/pricehistories/region': {
      get: {
        tags: ['PriceHistory'],
        summary: 'Retorna todas as informações importadas por sigla da região',
        parameters: [
          {
            name: 'Authorization',
            in: 'header',
            required: true,
            description: 'Token da sessão',
          },
          {
            name: 'region',
            in: 'query',
            required: true,
            description: 'Sigla da região',
            type: 'string',
          },
          {
            name: 'page',
            in: 'query',
            required: false,
            description: 'Número da página',
            type: 'int',
          },
          {
            name: 'limit',
            in: 'query',
            required: false,
            description: 'Limite de dados por requisição',
            type: 'int',
          },
          {
            name: 'offset',
            in: 'query',
            required: false,
            description: 'Pegar os dados à patir desse número',
            type: 'int',
          },
        ],
      },
    },
    '/pricehistories/distributor': {
      get: {
        tags: ['PriceHistory'],
        summary: 'Retorna os dados agrupados por distribuidora',
        parameters: [
          {
            name: 'Authorization',
            in: 'header',
            required: true,
            description: 'Token da sessão',
          },
          {
            name: 'distributor_id',
            in: 'query',
            required: true,
            description: 'Sigla da região',
            type: 'int',
          },
          {
            name: 'page',
            in: 'query',
            required: false,
            description: 'Número da página',
            type: 'int',
          },
          {
            name: 'limit',
            in: 'query',
            required: false,
            description: 'Limite de dados por requisição',
            type: 'int',
          },
          {
            name: 'offset',
            in: 'query',
            required: false,
            description: 'Pegar os dados à patir desse número',
            type: 'int',
          },
        ],
      },
    },
    '/pricehistories/collectiondate': {
      get: {
        tags: ['PriceHistory'],
        summary: 'Retorna os dados agrupados pela data da coleta',
        parameters: [
          {
            name: 'Authorization',
            in: 'header',
            required: true,
            description: 'Token da sessão',
          },
          {
            name: 'collection_date',
            in: 'query',
            required: true,
            description: 'Data da coleta',
            type: 'date',
          },
          {
            name: 'page',
            in: 'query',
            required: false,
            description: 'Número da página',
            type: 'int',
          },
          {
            name: 'limit',
            in: 'query',
            required: false,
            description: 'Limite de dados por requisição',
            type: 'int',
          },
          {
            name: 'offset',
            in: 'query',
            required: false,
            description: 'Pegar os dados à patir desse número',
            type: 'int',
          },
        ],
      },
    },
    '/pricehistories/cities/avg': {
      get: {
        tags: ['PriceHistory'],
        summary:
          'Retorna o valor médio do valor da compra e do valor da venda por município',
        parameters: [
          {
            name: 'Authorization',
            in: 'header',
            required: true,
            description: 'Token da sessão',
          },
          {
            name: 'page',
            in: 'query',
            required: false,
            description: 'Número da página',
            type: 'int',
          },
          {
            name: 'limit',
            in: 'query',
            required: false,
            description: 'Limite de dados por requisição',
            type: 'int',
          },
          {
            name: 'offset',
            in: 'query',
            required: false,
            description: 'Pegar os dados à patir desse número',
            type: 'int',
          },
        ],
      },
    },
    '/pricehistories/providers/avg': {
      get: {
        tags: ['PriceHistory'],
        summary:
          'Retorna o valor médio do valor da compra e do valor da venda por bandeira',
        parameters: [
          {
            name: 'Authorization',
            in: 'header',
            required: true,
            description: 'Token da sessão',
          },
          {
            name: 'page',
            in: 'query',
            required: false,
            description: 'Número da página',
            type: 'int',
          },
          {
            name: 'limit',
            in: 'query',
            required: false,
            description: 'Limite de dados por requisição',
            type: 'int',
          },
          {
            name: 'offset',
            in: 'query',
            required: false,
            description: 'Pegar os dados à patir desse número',
            type: 'int',
          },
        ],
      },
    },
  },
};
